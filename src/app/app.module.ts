import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';



import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';

import {MatToolbarModule} from '@angular/material/toolbar';

import {MatIconModule} from '@angular/material/icon';

import {MatSidenavModule} from '@angular/material/sidenav';

import {MatListModule} from '@angular/material/list';

import { HOMEComponent } from './home/home.component';

import { RouterModule, Routes } from '@angular/router';

import { GalleryComponent } from './gallery/gallery.component';
import { SettingsComponent } from './settings/settings.component';



const routes: Routes = [{path:'home',component:HOMEComponent},{path:'gallery',component:GalleryComponent},];



@NgModule({

  declarations: [

    AppComponent,

    HOMEComponent,

    GalleryComponent,
     SettingsComponent


  ], 
  imports: [

    BrowserModule,

    AppRoutingModule,

    BrowserAnimationsModule,

    MatButtonModule,

    MatToolbarModule,

    MatIconModule,

    MatSidenavModule,

    MatListModule,

    RouterModule.forRoot(routes)

  ],

  providers: [],

  bootstrap: [AppComponent]

})

export class AppModule { }